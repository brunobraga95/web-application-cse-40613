/**
 * Created by Bruno Braga on 05/02/2016.
 */
function convert_normal(u,v){
    var s = Math.sqrt(-2*Math.log(u))*Math.cos(2*Math.PI*v);
    var t = Math.sqrt(-2*Math.log(u))*Math.sin(2*Math.PI*v);
    return res={
        s:s,
        t:t
    }
}

function generate(number){
    for(i=0;i<number/2;i++){
        var u = Math.random();
        var v = Math.random();
        if(u!=0){
            normal = convert_normal(u,v);
            console.log(normal.s);
            console.log(normal.t);
        }
        else{
            console.log("u=0 generated, starting again...");
            i = -1;
        }
    }
}
generate(20);
