/**
 * Created by Bruno Braga on 05/02/2016.
 */

function Car(make, model, year, owner){
    this.make = make;
    this.model = model;
    this.year = year;
    this.owner = owner;
}

function Person(name, cars){
    this.name = name;
    this.cars= cars;
}

var car1 = new Car("Chevy", "Volt", "2013", "Gary");
var car2 = new Car("Dodge", "Ram", "2009", "Susan");
var car3 = new Car("Tesla", "X P90D", "2016", "Bob");
var car4 = new Car("Toyota", "Tacoma", "2004", "Gary");
var car5 = new Car("Ford", "Mustang", "2014", "Bob");
var car6 = new Car("Subaru", "Forester", "2007", "Alice");
var car7 = new Car("Audi", "TT", "2012", "Bob");
var car8 = new Car("Toyota", "Camry", "1995", "Susan");
var car9 = new Car("Ford", "Taurus", "1998", "Alice");
var car10 = new Car("BMW", "M4", "2011", "Alice");
var car11 = new Car("Acura", "TL", "2009", "Guillermo");

var p1 = new Person("Gary", [car1, car4]);
var p2 = new Person("Susan", [car2, car8]);
var p3 = new Person("Bob", [car3, car5, car7]);
var p4 = new Person("Alice", [car6, car9, car10]);
var p5 = new Person("Guillermo", [car11]);

console.log("Cars owned by %s", p1.name);
console.log(p1.cars);
console.log("\nCars owned by %s", p2.name);
console.log(p2.cars);
console.log("\nCars owned by %s", p3.name);
console.log(p3.cars);
console.log("\nCars owned by %s", p4.name);
console.log(p4.cars);
console.log("\nCars owned by %s", p5.name);
console.log(p5.cars);
