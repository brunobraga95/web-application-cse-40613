/**
 * Created by Bruno Braga on 05/02/2016.
 */
function check_palindrome(word){
    for(k=0;k<word.length;k++){
       if(word[k] != word[word.length-1-k])return false;
    }
    return true;
}

function palindrome(array_words){
    for(i=0;i<array_words.length;i++){
        var word = array_words[i];
        word.toLowerCase();
        len = word.length;
        var word_nospaces= "";
        for(j=0;j<len;j++){
            if(word[j] != ' '){
                word_nospaces+=word[j];
            }
        }
        if(check_palindrome(word_nospaces))console.log("Palindrome");
        else console.log("Not a Palindrome");
    }
}
palindrome(["ar  ar a  ","teste","aaaaa"]);