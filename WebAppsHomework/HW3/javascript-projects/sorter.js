/**
 * Created by Bruno Braga on 05/02/2016.
 */
function remove(array){
    var i;
    if(array.length <= 1)return array;
    for(i = 1;i<array.length;i++){
        if(array[i] == array[i-1])array.splice(i,1);
    }
    return array;
}
function sorter(vector){
    vector.sort();
    remove(vector);
    console.log(vector);
}
sorter(["2","1","2","5","1","6","0"]);
