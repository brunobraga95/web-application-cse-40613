"use strict";
var order_id = undefined;

$(function() {
    var firebase = new Firebase("https://brunobraga.firebaseio.com/");
    var curent_restaurant = undefined;
    var likesText = function(n) {
        if (!n) {
            n = 0;
            return "Be the first to like this!";
        }
        else if (n === 1) {
            return "1 person likes this!";
        }
        else {
            return n + " people like this!";
        }
    };

    var createReview = function(name, msg, url) {
        console.log(name, msg, url);
        var $li = $("<li class='review'>" +
            "<div class='review-block'><h3>" + name + "</h3>" +
            "<p>" + msg + "</p></div>" +
            "</li>");
        if (url) {
            $li.append($("<img src='" + url + "' alt='review-image' class='review-img' />"));
        }
        return $li;
    };

    function Restaurant(key, data) {
        console.log("Creating a new restaurant");
        var rid, name, street, city, state, zip, phone, site, menu, reviews;

        rid = key;
        name = data.name;
        street = data.street;
        city = data.city;
        state = data.state;
        zip = data.zip;
        phone = data.phone;
        site = data.site;
        menu = data.menu;
        reviews = data.reviews;


        console.log("Creating restaurant ", name);

        function createDetailHeader() {
            var addr2 = city + ", " + state + " " + zip;
            return $("<div class='restaurant-detail-header'>" +
                "<div class='restaurant-addr-block'><p>" + street + "</p><p>" + addr2 + "</p></div>" +
                "<div class='restaurant-contact-block'><p>" + phone + "</p><a href='" + site + "'>" + site + "</a></div>" +
                "</div>");
        }

        function createDetailBody() {
            var i, $detailBody = $("<div class='restaurant-detail-body'><dl class='restaurant-menu'></dl></div>");

            for (i = 0; i < menu.length; i++) {
                $detailBody.children().first().append(createMenuItem(i));
            }

            $detailBody.append(createReviewSection());

            return $detailBody;
        }

        function createMenuItem(i) {
            var $item, $likeButton, $orderButton, likeMessage;
            if (!menu[i].likes) {
                menu[i].likes = 0;
            }

            likeMessage = likesText(menu[i].likes);

            $item = $("<div class='menu-item-block'>" +
                "<dt class='menu-item-name'>" + menu[i].name + "</dt>" +
                "<dd class='menu-item-desc'>" + menu[i].description + "</dd>" +
                "<dd class='menu-item-price'>$" + menu[i].price + "</dd>" +
                "</div>");
            $likeButton = $("<button class='like-button'>" + likeMessage + "</button>");
            $likeButton.data({rid: rid, index: i, likes: menu[i].likes});

            $orderButton = $("<button class='order-button'>Order</button>");
            $orderButton.data({rid: rid, index: i});
            firebase.child("restaurants").child(rid).child("menu").child(i).on("value", function(snapshot) {
                $likeButton.text(likesText(snapshot.val().likes));
            });

            $item.children().last().append($likeButton);
            $item.children().last().append($orderButton);

            return $item;
        }
        $('.order-button').hide();


        function createReviewSection() {
            var $section, $form, $reviewsList, $submit;

            $form = $("<form class='review-form'>" +
                "<label for='name-field'>Name:</label>" +
                "<input type='text' class='name-field' placeholder='ex. Customer Consumerson' />" +
                "<br />" +
                "<label for='file-field'>Photo:</label>" +
                "<input type='file' class='file-field' />" +
                "<br />" +
                "<textarea class='review-field' placeholder='What did you think?'></textarea>" +
                "<br />" +
                "</form>");

            $reviewsList = createReviewList();
            $submit = $("<input type='submit' class='review-submit' value='Submit'/>");
            $form.append($submit);
            $section = $("<div class='reviews-section'></div>");
            $section.append($form).append($reviewsList);

            $form.data({rid: rid});

            return $section;
        }

        function createReviewList() {
            var $list, $li, i;
            console.log(reviews);
            $list = $("<ul class='reviews-list'></ul>");

            for (i in reviews) {
                if (reviews.hasOwnProperty(i)) {
                    $li = createReview(reviews[i].name, reviews[i].review, reviews[i].url);
                    $list.append($li);
                }
            }

            return $list;
        }

        function createDetail() {
            return $("<section class='restaurant-detail'></section>").append(createDetailHeader()).append(createDetailBody());
        }

        this.createThumbnail = function(parent) {
            parent.append($("<section id='" + rid + "' class='restaurant-thumbnail'><h2>" + name + "</h2></section>").data(this).append(createDetail()));
        };

    }

    //  Load initial restaurant list
    firebase.child("restaurants").once("value", function(snapshot){
        var restaurants, restaurant, temp;

        restaurants = snapshot.val();
        console.log(restaurants);

        for (restaurant in restaurants) {
            if (restaurants.hasOwnProperty(restaurant)) {
                temp = new Restaurant(restaurant, restaurants[restaurant]).createThumbnail($("#restaurants"));
            }
        }
    });

    //
    //  Handle showing/hiding restaurant details
    //
    $("#restaurants").on("click", ".restaurant-thumbnail", function(e) {
        e.preventDefault();                    // Prevent default action of button
        e.stopPropagation();
        $(this)                                // Get the element the user clicked on
            .children(".restaurant-detail")    // Select child panel
            .not(':animated')                  // If it is not currently animating
            .slideToggle();
        return false;
    });

    //
    //  Handle liking menu item
    //
    $("#restaurants").on("click", ".like-button", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $e, menuItemRef, rid, index;
        console.log("Clicked Like!")
        $e = $(e.target);
        rid = $e.data("rid");
        index = $e.data("index");
        menuItemRef = firebase.child("restaurants").child(rid).child("menu").child(index);
        menuItemRef.child("likes").transaction(function(currentLikes) {
            if (currentLikes) {
                return +currentLikes + 1;
            }
            return 1;
        });

        return false;
    });

    $("#restaurants").on("click", ".order-button", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $e, menuItemRef, rid, index;
        console.log("Clicked order!")
        $e = $(e.target);
        rid = $e.data("rid");
        if(current_restaurant == undefined)current_restaurant = rid;
        else{
            if(current_restaurant != rid){
                alert("You can not Order From a different Restaurant");
                return 1;
            }
        }
        index = $e.data("index");
        menuItemRef = firebase.child("restaurants").child(rid).child("menu").child(index);
        var prato;
        menuItemRef.child("name").transaction(function(p) {
            if (p) {
                prato = p;
                return p;
            }
            return 1;
        });

        var price;
        menuItemRef.child("price").transaction(function(pr) {
            if (pr) {
                price = +pr;
                return pr;
            }
            return 1;
        });
        $("#dialog").empty();
        $("#dialog").append("<p style='align-content: center'>"+prato+"<!doctype html></p>");
        $("#dialog").append("<p style='align-content: center'>Price:"+price+"<!doctype html></p>");
        $("#dialog").append("<label for=\"login-email\" class=\"left\">Quantity</label>");
        $("#dialog").append("<input type=\"number\" min = \"1\" id=\"number-items\" placeholder=\"1\" required>");
        $("#number-items")[0].value = 1;

        $("#dialog").dialog({
            modal: true,
            resizable: false,
            open: function(event, ui) { jQuery('.ui-dialog-titlebar-close').hide(); },
            buttons: {
                "ADD": function() {
                    cart_items = 1;
                    var firebase_orders = new Firebase("https://brunobraga.firebaseio.com");
                    var ordersRef = firebase_orders.child("orders").child(global_user_uid);
                    var ordersRefItem = ordersRef;
                    if($("#number-items")[0].value == undefined || $("#number-items")[0].value < 1 ){
                        alert("Please Choose a number greater or equal than 1");
                        return 1;
                    }
                    if(order_id == undefined)
                    {
                        ordersRef = ordersRef.push();
                        ordersRef.set({
                            "restaurant_id":rid,
                            "total_items": $("#number-items")[0].value,
                            "total_price": Number($("#number-items")[0].value)*Number(price),
                            "submitted":"false"
                        }, function(error) {
                            if (error) {
                                instance.onError(error);
                                console.log(error);
                            }
                        });
                        console.log(ordersRef.key());
                        order_id = ordersRef.key();

                        var itemsInfo = {
                            "price":price,
                            "quantity":+$("#number-items")[0].value
                        }

                        ordersRefItem = ordersRefItem.child(ordersRef.key()).child("items").child(prato);
                        ordersRefItem.set({
                            "price":price,
                            "quantity":$("#number-items")[0].value
                        }, function(error) {
                            if (error) {
                                instance.onError(error);
                                console.log(error);
                            }
                        });
                    }
                    else{
                        ordersRefItem.child(order_id).child("total_items").transaction(function(currentTotalItems){
                            if(currentTotalItems){
                                return Number(currentTotalItems)+Number($("#number-items")[0].value);
                            }
                            return Number(currentTotalItems);
                        });
                        ordersRefItem.child(order_id).child("total_price").transaction(function(currentTotalPrice){
                            if(currentTotalPrice){
                                return Number(currentTotalPrice)+Number($("#number-items")[0].value)*Number(price);
                            }
                            return Number(currentTotalPrice);
                        })
                        ordersRefItem = ordersRefItem.child(order_id).child("items").child(prato);
                        ordersRefItem.child("price").transaction(function(currentPrice) {
                            if (currentPrice) {
                                return currentPrice;
                            }
                            return price;
                        });
                        ordersRefItem.child("quantity").transaction(function(currentQuantity) {
                            if (currentQuantity) {
                                var new_quantity = Number(currentQuantity)+Number($("#number-items")[0].value);
                                return Number(currentQuantity)+Number($("#number-items")[0].value);
                            }
                            return Number($("#number-items")[0].value);
                        });
                    }

                    $(this).dialog("close");
                },
                "CANCEL": function() {
                    $(this).dialog("close");
                    //window.location.href = "contact.html";
                }
            }


        })
        return false;
    });

    //
    //  Prevent form interaction from closing the accordion
    //
    $("#restaurants").on("click", ".name-field, .review-field", function(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $("#restaurants").on("click", ".file-field, .review-submit", function(e) {
        e.stopPropagation();
    });

    $("#restaurants").on("submit", ".review-form", function(e) {
        e.preventDefault();
        console.log("Caught the review submit");
        $(e.target).parent(".reviews-section").trigger("submit", $(e.target).data());
    });

    //
    //  Handle review submission
    //
    $("#restaurants").on("submit", ".reviews-section", function(e, rdata) {
        e.preventDefault();
        e.stopPropagation();

        console.log("Submitting review...")

        AWS.config.credentials = new AWS.Credentials("AKIAITSVXAMU4TUFCM7Q","fg15r/LnZ9TWeZ9XncomAauju/SYZheKnIJM84TE");
        AWS.config.region = "us-east-1";

        var $section, $form, $list,  $name, name, $msg, msg, $file, file, reviewRef, review;
        var params, s3URL, bucket = new AWS.S3({ params: {"Bucket": "bdejesus" }});

        $section = $(e.target);
        $form = $($section.find(".review-form"));

        //rdata = $form.data();
        console.log(rdata);

        $name = $form.find(".name-field");
        $msg = $form.find(".review-field");
        $file = $($form.find(".file-field"));

        name = $name.val();
        msg = $msg.val();

        if (name !== "" && msg !== "") {
            if ($file.prop("files").length > 0) {
                console.log("Uploading photo");
                file = $file.prop("files")[0];
                params = {Key: file.name, ContentType: file.type, Body: file, ACL: "public-read"};

                console.log(params);

                bucket.upload(params, function(err, data) {
                    console.log(data);
                    if (err) {
                        console.log(err);
                    }
                    else {
                        s3URL = data["Location"];
                        review = {name: name, review: msg, url: s3URL};
                        reviewRef = firebase.child("restaurants").child(rdata.rid).child("reviews");
                        //if ($("#reviews-list").children().length > 0) {
                        //    reviewRef.push(review);
                        // }
                        //else {
                        reviewRef.child(btoa(Date.now())).set(review);
                        //}
                        $list = $($section.find(".reviews-list"));
                        $list.prepend(createReview(name, msg, s3URL));
                    }
                });
            }

            else {
                review = {name: name, review: msg};
                reviewRef = firebase.child("brunobraga").child(rdata.rid).child("reviews");
                reviewRef.child(btoa(Date.now())).set(review, function(error) {
                    console.log(error);
                });
                $list = $($section.find(".reviews-list"));
                $list.prepend(createReview(name, msg, ""));
            }

            $name.val("");
            $msg.val("");
            $file.val("");
        }

        false;
    });

});
