"use strict";

/* Implement String.validate() here */

String.prototype.validate = function(minimum,maximum,expression,notexpression){
    expression+= minimum;
    expression+=","
    expression+= maximum;
    expression+="}$";
    var regex = new RegExp(expression, 'g');
    if(this.match(regex) && !this.match(notexpression))console.log("match");
    else console.log("does not match");
}

/* Implement String.validate() here */

var test_passwords = [
    "Hxou7p&&3",
    "password",
    "j3Vla$!",
    ";larJ7",
    "DROWSSAP",
    "K33pI7S@f3",
    ":Belieber1"
];
var inval = /[^!$%&#;?@~a-zA-Z0-9]|\s+/;
test_passwords[0].validate(8,20,"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*]).{",inval);

