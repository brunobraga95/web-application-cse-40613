/**
 * Created by Bruno Braga on 10/02/2016.
 */

console.log("Bitcoin");
var Firebase = require('firebase');
var bitcoinRef = new Firebase("https://publicdata-cryptocurrency.firebaseio.com/bitcoin");

function bid(s){
    console.log("Bid:",s.val());
}
function ask(s){
    console.log("Ask:",s.val());
}

function last(s){
    console.log("Last:",s.val());
}
function interval(){
    bitcoinRef.child("bid").once("value", bid);
    bitcoinRef.child("ask").once("value",ask);
    bitcoinRef.child("last").once("value",last);

}
var time = 1000;
setInterval(interval,time);