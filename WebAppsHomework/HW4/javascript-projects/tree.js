// Establish a parent class
/*
In this exercise, I did not use DFS for the search, I DO know how to use it
but since I implemented a binary search tree(smaller value to the left, bigger value to the right)
I decided to use the efficient BST query algorithm. To implement the DFS all I should do it keep going left recursivily
 and then go right.
 */
function TreeNode(data,count){
    this.data = data;
    this.count = count;
    this.left = undefined;
    this.right = undefined;
    this.compare = function(value){
        if(this.data == value)return this;
        if(this.data < value) return this.right;
        else return this.left;
    }
}

// Establish a child class
function BinaryTree(){
    this.root = 0;
    this.insert = function(value){
        if(this.root == 0){
            this.root = new TreeNode(value,1);
        }
        else this.insert_aux(value,this.root);
    }
    this.insert_aux = function(value,node){
        if(node == undefined)return;
        if(value == node.data){
            node.count++;
            return;
        }
        if(value >= node.data){
            if(node.right == undefined)node.right = new TreeNode(value,1);
            else this.insert_aux(value,node.right);
        }
        else{
            if(node.left == undefined)node.left = new TreeNode(value,1);
            else this.insert_aux(value,node.left);
        }
    }
    this.print = function(node){
        console.log("****values****");
        this.print_aux(this.root);
        console.log("****end of values****");
    }
    this.print_aux = function(node){
        if(node == undefined)return;
        this.print_aux(node.left);
        if(node.count >0)console.log(node.data);
        this.print_aux(node.right);
    }
    this.remove = function(value,node){
        if(node == undefined)this.remove(value,this.root);
        else{
            if(node.data == value){
                node.count--;
                return;
            }
            if(node.right!=undefined)this.remove(value,node.right);
            if(node.left!=undefined)this.remove(value,node.left);
        }
    }
    this.find = function (value,node) {
        if(node == undefined){
           var auxiliar = this.root.compare(value);
           if(auxiliar == undefined)return false;
           if(auxiliar == this.root)return true;
           else{
                return this.find(value,auxiliar);
           }
        }
        else{
            var auxiliar = node.compare(value);
            if(auxiliar == undefined)return false;
            else if(auxiliar == node)return true;
            else{
                return this.find(value,auxiliar);
            }
        }
    }
}
BinaryTree.prototype = new TreeNode();
var tree = new BinaryTree();

var values = [32, 1, 53, 4, 6, 16, 2, 5, 5, 7, 3, 11, 23]

var i;

for (i = 0 ; i < values.length; i++) {
    tree.insert(values[i]);
}

tree.print();

tree.remove(4);

tree.print();

tree.remove(5);

tree.print();

tree.insert(11);

tree.print();

if (tree.find(53)) {
    console.log("Found 53");
}

else {
    console.log("Error: did not find 53");
}

if (!tree.find(111)) {
    console.log("111 not in the tree");
}
else {
    console.log("Found 111");
}
