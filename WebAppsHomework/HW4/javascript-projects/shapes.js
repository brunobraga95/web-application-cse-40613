
// Establish a parent class
function Shape(){
    var points = arguments;
    this.test = arguments;
    this.setpoint = function(){
        this.test = arguments;
    }
    this.perimeter = function(){
        var j;
        var perimeter = 0, x, y;
        for(j = 0;j < this.test.length;j++){
            x = this.test[j][0] - this.test[(j+1)%this.test.length][0];
            x = x*x;
            y = this.test[j][1] - this.test[(j+1)%this.test.length][1];
            y = y*y;
            perimeter+= Math.sqrt(x+y);
        }
        return perimeter;

    }
}

// Establish a child class
function Triangle(){
    var points = arguments;
    this.test = arguments;
    this.root;
    this.area = function(){
        this.root = new Shape([0,0], [0,7], [3,4], [1,2]);
        var p = this.perimeter();
        p = p/2;
        x = this.test[0][0] - this.test[1][0];
        x = x*x;
        y = this.test[0][1] - this.test[1][1];
        y = y*y;
        a = Math.sqrt(x+y);

        x = this.test[1][0] - this.test[2][0];
        x = x*x;
        y = this.test[1][1] - this.test[2][1];
        y = y*y;
        b = Math.sqrt(x+y);

        x = this.test[2][0] - this.test[0][0];
        x = x*x;
        y = this.test[2][1] - this.test[0][1];
        y = y*y;
        c = Math.sqrt(x+y);

        return Math.sqrt(p*(p-a)*(p-b)*(p-c));

    }
}

function Rectangle(){
    var points = arguments;
    this.test = arguments;
    this.area = function(){
        x = this.test[0][0] - this.test[1][0];
        x = x*x;
        y = this.test[0][1] - this.test[1][1];
        y = y*y;
        a = Math.sqrt(x+y);
        x = this.test[1][0] - this.test[2][0];
        x = x*x;
        y = this.test[1][1] - this.test[2][1];
        y = y*y;
        b = Math.sqrt(x+y);

        return (a*b);
    }
}
Triangle.prototype = new Shape();
Rectangle.prototype = new Shape();

// Make the Childclass inherit all of the Parent class characteristics
// by using the prototype property, explained in depth here: youtube.com/watch?v=EBoUT2eBlT4

var shape = new Shape([0,0], [0,7], [3,4], [1,2]);

var tri = new Triangle([0,0],[2,1],[2,0]);

var rect  = new Rectangle([0,0], [1,0], [1,1], [0,1]);

var perimeter, area;

if (shape.hasOwnProperty("perimeter")) {
    perimeter = shape.perimeter();
}
else {
    perimeter = "No perimeter attribute"
}

if (shape.hasOwnProperty("area")) {
    area = shape.area();
}
else {
    area = "No area attribute"
}

console.log("shape", perimeter, area);

var perimeter, area;

if (tri.hasOwnProperty("perimeter")) {
    perimeter = tri.perimeter();
}
else {
    perimeter = "No perimeter attribute"
}

if (tri.hasOwnProperty("area")) {
    area = tri.area();
}
else {
    area = "No perimeter attribute"
}

console.log("tri", perimeter, area);


 var perimeter, area;
/*
Classes the Inherent from other can use their methods
but the hasOwnProperty will still return false
*/

 if (rect.hasOwnProperty("perimeter")) {
 perimeter = rect.perimeter();
 }
 else {
 perimeter = "No perimeter attribute"
 }

 if (rect.hasOwnProperty("area")) {
 area = rect.area();
 }
 else {
 area = "No perimeter attribute"
 }

 console.log("rect", perimeter, area);
