/**
 * Created by Bruno Braga on 10/02/2016.
 */
var clock = function(){
    var d, h, m, s,step;
    var alarmObject = {
        hourer:0,
        minute:0,
        second:0
    };
    var alarms = new Array();
    return{
        time:function(){
            d = new Date(); // for now
            h = d.getHours(); // => 9
            m = d.getMinutes(); // =>  30
            s = d.getSeconds(); // => 51

        },
        addAlarm: function(data){
            var new_alarm = Object.create(alarmObject);
            new_alarm.hourer = data.getHours();
            new_alarm.minute = data.getMinutes();
            new_alarm.second = data.getSeconds();
            alarms.push(new_alarm);
        },
        showTime:function()
        {
            console.log("TIME: "+h+":"+m+":"+s);
        },
        timestep:function(passo){
            step = passo;
        },
        alarm:function(alarm_hourer,alarm_minute,alarm_second){
            console.log("**** Alarm WAKE UP: ",alarm_hourer+":"+alarm_minute+":"+alarm_second+"****");
        },
        alarms:function(alarmes){
            var k =0;
            for(k = 0;k<alarmes.length;k++){
                this.addAlarm(alarmes[k]);
            }
        },
        tick:function(){
            s+=step/1000;
            if(s>=60){
                s=s%60;

                m++;
                if(m >=60){
                    m = 0;
                    h++;
                    if(h >= 24)h = 0;
                }
            }
            var i;
            for(i = 0;i<alarms.length;i++){
                if(h === alarms[i].hourer && m === alarms[i].minute && (s-5 <= alarms[i].second && s> alarms[i].second )){
                    this.alarm(alarms[i].hourer,alarms[i].minute,alarms[i].second);
                }else  this.showTime();

             }

        }
    };
};

var clock1 = new clock();
var passo = 1000;
clock1.timestep(passo);
clock1.time();
setInterval(function() {
    clock1.tick();
}, passo);

//SET YOUR ALARMS BELOW
var alarmes = new Array();
var new_alarm = new Date();
new_alarm.setSeconds(15);
new_alarm.setMinutes(51);
new_alarm.setHours(14);
alarmes.push(new_alarm);


var new_alarm2 = new Date();
new_alarm2.setSeconds(20);
new_alarm2.setMinutes(51);
new_alarm2.setHours(15);
alarmes.push(new_alarm2);
/*
The alarms keep "ringing" at the minimum of 5 seconds, dependending on the clock step.
*/

var new_alarm3 = new Date();
new_alarm3.setSeconds(20);
new_alarm3.setMinutes(10);
new_alarm3.setHours(15);
alarmes.push(new_alarm3);
clock1.alarms(alarmes);
//window.setInterval(clock1.tick(),passo);
