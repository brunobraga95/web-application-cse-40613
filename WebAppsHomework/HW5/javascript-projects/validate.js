"use strict";

/* Implement String.validate() here */
function sucess(password){
    console.log("%s is a valid Password\n",password);
}
function error (password,len,uppercase,lowecase,number,special,whitespace,newline,tabs,minimum,maximum){
    console.log("%s Produces the following errors:\n",password);
    if(!len){
        console.log("Error: Password must have between %d and %d characters\n",minimum,maximum);
    }
    if(!uppercase){
        console.log("Error: Password must have at least one uppercase letter\n");
    }
    if(!lowecase){
        console.log("Error: Password must have at least one lowercase letter\n");
    }
    if(!number){
        console.log("Error: Password must have at least one number\n");
    }
    if(!special){
        console.log("Error: Password must have at least one special character\n");
    }
    if(whitespace){
        console.log("Error: Password cannot contain white spaces\n");
    }
    if(newline){
        console.log("Error: Password cannot contain new line\n");
    }
    if(tabs){
        console.log("Error: Password cannot contain tabs\n");
    }
}
String.prototype.validate = function(minimum,maximum,expression,notexpression,error,sucess){
    expression+= minimum;
    expression+=","
    expression+= maximum;
    expression+="}$";
    var regex = new RegExp(expression, 'g');
    if(this.match(regex) && !this.match(notexpression))sucess();
    else{
        var len = true,uppercase = false,lowercase = false,number=false,special = false,whitespace = false;
        var newline = false,tabs = false;
        if(this.length<minimum || this.length>maximum){
            len = false;
        }

        var i;
        for(i=0;i<this.length;i++){
            if(this.charAt(i)>='A' && this.charAt(i)<='Z' )uppercase = true;
            if(this.charAt(i)>='a' && this.charAt(i)<='z' )lowercase = true;
            if(this.charAt(i)>='0' && this.charAt(i)<='9' )number= true;
            if(this.charAt(i)==='!' || this.charAt(i)==='$' || this.charAt(i)==='%' || this.charAt(i)==='&' || this.charAt(i)==='#' || this.charAt(i)===';' || this.charAt(i)==='?' || this.charAt(i)==='@' || this.charAt(i)==='~' )special= true;
            if(this.charAt(i) === ' ')whitespace= true;
            if(this.charAt(i) === '\n')newline = true;
            if(this.charAt(i) === '\t')tabs = true;
        }
        error(this,len,uppercase,lowercase,number,special,whitespace,newline,tabs,minimum,maximum);


    }
}
this.lowercase= function(){
    console.log("Error: Password must have at least one lowercase letter\n");
    return 0;
}

/* Implement String.validate() here */

var test_passwords = [
    "Hxou7p&&3",
    ";larJ7",
    "j3Vla$!",
    ";larJ7",
    "DROWSSAP",
    "K33pI7S@f3",
    "ealkj\naslk \nasd"
];
var inval = /[^!$%&#;?@~a-zA-Z0-9]|\s+/;
//var error = new error();
test_passwords[6].validate(8,20,"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*]).{",inval,error,sucess);

