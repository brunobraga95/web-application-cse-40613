// This is supposed to work on simple.html
// in the homework 6 starter code

// Write code to remove an item on double click
// add handler to the ul
// print the item's text() and html() to the console
$(function(){
    $("ul").dblclick(function() {
        var timestamp = event.timeStamp;
        var id ="#"+ event.target.id;
        $(id).hide();
        var text = $(id).text();
        var html = $(id).html();
        console.log(text);
        console.log(html);
        console.log(timestamp);
    });

});
