/**
 * Created by Bruno Braga on 22/02/2016.
 */
$(function(){
    var ref = new Firebase("https://brunobraga.firebaseio.com/restaurants");
    ref.on("value", function(snapshot) {
        var id;
        var len = 0;
        var flag = 0;
        for (var o in snapshot.val()) {
            len++;
        }
        var j=0;
        var arr = new Array(len);
        var arr_ids = new Array(len);
        for(j = 0;j<len;j++){
            arr[j] = snapshot.val()[Object.keys(snapshot.val())[j]];
            var id = "id_"+j;
            arr_ids[j] = id;
            var add = "<li id = \""+ id + "\"><p>"+arr[j].name+"</p></li>";
            $("#restaurants-list").append(add);
        }

        $("#wrap").dblclick(function() {
            flag = 0;
            $("#info_left").empty();
            $("#info_right").empty();
            $("li").remove();
            for(j = 0;j<len;j++){
                arr[j] = snapshot.val()[Object.keys(snapshot.val())[j]];
                var id = "id_"+j;
                arr_ids[j] = id;
                var add = "<li id = \""+ id + "\"><p>"+arr[j].name+"</p></li>";
                $("#restaurants-list").append(add);
            }
        });

        $("ul").on('click','li',function(event) {
            // your code here ...
            id ="#"+ event.target.id;
            if(event.target.id != ""){
                for(j=0;j<len;j++){
                    var id_hide = "#id_"+j;
                    if(id_hide!=id)$(id_hide).hide();
                }
                var pos = id.charAt(id.length-1);
                var rest = arr[pos];
                console.log(rest);
                var len2 = 0;
                for (var o in rest) {
                    len2++;
                }
                if(flag == 0){
                var add_info = "<p><b>City: "+rest[Object.keys(rest)[0]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>Lat: "+rest[Object.keys(rest)[1]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>Ing: "+rest[Object.keys(rest)[2]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>Club: "+rest[Object.keys(rest)[4]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>Phone: "+rest[Object.keys(rest)[5]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>site: "+rest[Object.keys(rest)[6]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>State: "+rest[Object.keys(rest)[7]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>Street: "+rest[Object.keys(rest)[8]]+"</b></p>"
                $("#info_left").append(add_info);

                var add_info = "<p><b>Zip: "+rest[Object.keys(rest)[9]]+"</b></p>"
                $("#info_left").append(add_info);
                var items_ob = rest[Object.keys(rest)[3]];
                console.log("items_ob",items_ob);
                $("#info_right").append("<h3 id = \"menu\">Menu</h3>");
                $("#menu").css("background-color","white");
                $("#menu").css("text-align","center");
                for(j=0;j<6;j++){
                    var items = items_ob[Object.keys(items_ob)[j]];

                    var add_info = "<p><b>Name:</b>  "+items.name+"</p>"
                    $("#info_right").append(add_info);

                    var add_info = "<p><b>Description:</b>"+items.description+"</b></p>"
                    $("#info_right").append(add_info);


                    var add_info = "<p><b>Price:</b>  "+items.price+"</p>"
                    $("#info_right").append(add_info);
                }
                    flag = 1;
                }
            }
        });

      }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });
});