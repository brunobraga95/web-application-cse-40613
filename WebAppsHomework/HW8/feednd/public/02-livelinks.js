/**
 * Created by jesus on 3/1/16.
 */
"use strict";
var global_user,global_user_uid,userCart = undefined;
var userEmail = undefined,userPassword = undefined,cart_items = undefined,current_restaurant = undefined;
function LiveLinks(fbname) {
    var firebase = new Firebase("https://"+ fbname + ".firebaseio.com");
    this.firebase = firebase;
    var usersRef = this.firebase.child('users');
    this.usersRef = usersRef;
    console.log("usersRef:"+this.usersRef);
    var uid;
    var linksRef = this.firebase.child('livelinks');
    this.linksRef = linksRef;
    var instance = this;

    //overridable functions
    this.onLogin = function(user) {};
    this.onLoginFailure = function() {};
    this.onLogout = function() {};
    this.onError = function(error) {};

    // long running firebase listener
    this.start = function() {
      firebase.onAuth(function (authResponse) {
          if (authResponse) {
              console.log("user is logged in");
              global_user_uid = authResponse.uid;
              console.lo
              usersRef.child(authResponse.uid).once('value', function(snapshot) {
                  instance.user = snapshot.val();
                  global_user = instance.user;
                  console.log(snapshot.val());
                  instance.onLogin(instance.user);
              });
          } else {
              console.log("user is logged out");
              instance.onLogout();
          }
      });
    };

    this.uid = function() {return uid;};
    // signup with an alias
    this.signup = function(email,password,password_confirmation,name,city,state,street,zip,phone) {
        this.firebase.createUser({
            email : email,
            password : password
        }, function(error, userData) {
            if (error) {
                instance.onError("Error creating user" + error);
                console.log(error);
            }
            else {
                instance.userData = userData;
                console.log("user data",userData);
                usersRef.child(userData.uid).set({
                    "addr" : {
                        "city":city,
                        "state":state,
                        "street":street,
                        "zip":zip
                    },
                    "email":email,
                    "name":name,
                    "phone":phone
                }, function(error) {
                    if (error) {
                        instance.onError(error);
                        console.log(error);
                    }
                    else {
                        instance.login(email,password);
                    }
                });
            }
        });
    };
    // login with email and password
    this.login = function(email,password) {
        this.firebase.authWithPassword({
            email: email,
            password : password
        }, function(error, authData) {
            if (!error) {
                instance.auth = authData;
                console.log("uid:", authData.uid);
            } else {
                instance.onError("login failed! " + error);
                instance.onLoginFailure();
            }
        }, {
            remember : "sessionOnly"
        });
    };
    this.loginFacebook = function(email,password) {
       this.firebase.authWithOAuthPopup("facebook", function(error, authData) {
            if (error) {
                console.log("Login Failed!", error);
            } else {
                console.log("Authenticated successfully with payload:", authData);
                console.log(authData.facebook.displayName);
                instance.auth = authData;
                console.log("uid:", authData.uid);
                instance.userData = authData,
                usersRef.child(authData.uid).set({
                    "name":authData.facebook.displayName,
                }, function(error) {
                    if (error) {
                        instance.onError(error);
                        console.log(error);
                    }

                });
               }
            },{
            remember : "sessionOnly"
        });
    };
    // logout
    this.logout = function() {
        this.firebase.unauth();
        instance.auth=null;
    };
}

$(function() {
    // Use your firebase address here:
    var ll = new LiveLinks("brunobraga");
    var $loginButton = $('#login-button'),
        $signupButton = $('#signup-button'),
        $logoutButton = $('#logout-button'),
        $loginForm = $('#login-form'),
        $signupForm = $('#signup-form'),
        $shopCartImage= $('#shop-cart-image'),
        $facebookLoginButton = $("#facebook-login-image"),
        $alerts;

    ll.onLogin = function(user) {
        showAlert("Welcome to LiveLinks "+user.name+"!","success");
        $loginButton.hide();
        $signupButton.hide();
        $logoutButton.show();
        $facebookLoginButton.hide();
        $shopCartImage.show();
        $signupForm.hide();
        $loginForm.hide();
        $('.order-button').show();
    };

    ll.onLogout = function() {
        console.log("in onLogout");
        $shopCartImage.hide();
        $loginButton.show();
        $facebookLoginButton.show();
        $signupButton.show();
        $logoutButton.hide();
        $loginForm.hide();
        $signupForm.hide();
        global_user = undefined;
        $('.order-button').hide();
        $('#shopDiv').hide();
        $('.restaurant-thumbnail').show();
    };

    ll.onLoginFailure = function() {
        console.log("in onLoginFailure");
        $loginButton.show();
        $signupButton.show();
    };

    $logoutButton.on('click',function(e) {
        $("#dialog").empty();
        $("#dialog").append("<p style='align-content: center'>Are you sure you want to logout?<!doctype html></p>");
        $("#dialog").attr("title","Going out?");
        $("#dialog").dialog({
            modal: true,
            resizable: false,
            open: function (event, ui) {
                jQuery('.ui-dialog-titlebar-close').hide();
            },
            buttons: {
                "Yes": function () {
                    ll.logout();
                    $logoutButton.hide();
                    $loginButton.show();
                    $signupButton.show();
                    location.reload();
                    $(this).dialog("close");
                },
                "Cancel": function () {
                    $(this).dialog("close");
                    //window.location.href = "contact.html";
                }
            }
        })
        return false;
    });


    ll.onError = function(error) {
        showAlert(error,"danger");
    };
    $loginButton.show();
    $signupButton.show();
    $logoutButton.hide();
    $loginForm.hide();
    $signupForm.hide();
    $shopCartImage.hide();
    $('.order-button').hide();
    $facebookLoginButton.show();

    // login forms
    $loginButton.on('click',function(e) {
        console.log("clicked login button!");
        $loginButton.hide();
        $signupButton.hide();
        $signupForm.hide();
        $loginForm.show();
        $('#login-email').val("").focus();
        $('#login-password').val("").blur();
        return false;
    });

    $loginForm.on('submit',function(e) {
        $loginForm.hide();
        e.preventDefault();
        e.stopPropagation();
        userEmail = $(this).find('#login-email').val();
        userPassword = $(this).find('#login-password').val();
            ll.login($(this).find('#login-email').val(), $(this).find('#login-password').val());
        $('#login-email').val("").blur();
        $('#login-password').val("").blur();
        return false;
    });

    $signupButton.on('click',function(e) {
        console.log("clicked signup button");
        $signupButton.hide();
        $loginButton.hide();
        $loginForm.hide();
        $signupForm.show();
        $('#signup-email').val("").focus();
        $('#signup-password').val("").blur();
        $('#signup-alias').val("").blur();
        return false;
    });

    $shopCartImage.on('click',function(e) {
        $('#shopDiv').remove();
        if(current_restaurant == undefined){
            alert("Your Cart is Empty");
            return 1;
        }
        if($shopCartImage.attr("src") == "backCart.ico"){
            $('#shopDiv').remove();
            $('.restaurant-thumbnail').show();
            $shopCartImage.attr("src","cart.png");
            return 1;
        }
        $shopCartImage.attr("src","backCart.ico");
        console.log("orderCart:",order_id);
        userCart = ll.firebase.child("orders").child(global_user_uid).child(order_id);
        console.log(userCart);
        var snapshot;
        var $item;
        $('.restaurant-thumbnail').hide();
        var $shopCartScreen = $("<div id = \"shopDiv\"></div>");
        $('#restaurants').append($shopCartScreen);
        userCart.on('value', function(notesSnapshot) {
            console.log(notesSnapshot.val());
            var $restaurant_order = $("<div id = \"cart_list\" class='restaurant-thumbnail-order'>" +
                "<h2>These are your orders for Restaurant "+notesSnapshot.val().restaurant_id+"</h2>");
            $('#shopDiv').append($restaurant_order);
            var items_ordered = notesSnapshot.val().items;

            for (var key in items_ordered) {
                if (items_ordered.hasOwnProperty(key)) {
                    console.log(items_ordered[key]);
                    console.log(items_ordered[key].price);

                    var $items_list = $("<div class='menu-item-block'>" +
                        "<dt class=\"menu-item-name\">"+key+"</dt><br>"+
                        "<dt class=\"menu-item-name\">Price: "+items_ordered[key].price+"</dt><br>"+
                        "<dt class=\"menu-item-name\">Quantity: "+items_ordered[key].quantity+"</dt><br>");
                    $('#cart_list').append($items_list);

                }
            }
            var $totalPrice = $("<div class='menu-item-block'><h3>The total Price: "+notesSnapshot.val().total_price+"</h3> <button class='submit-button' id=\"submitButton\">Submit Order</button></div>");
            $('#cart_list').append($totalPrice);
        });
        return false;
    });

    $("#restaurants").on("click", ".submit-button", function(e) {
        $('#shopDiv').remove();
        $('.restaurant-thumbnail').show();
        $shopCartImage.attr("src","cart.png");
        console.log("clicked submit button");
        userCart.update({
            "submitted": "true"
        });
        alert("Your order was submitted");
        cart_items = undefined;
        order_id = undefined;
        current_restaurant = undefined;

        return false;
    });
    $signupForm.on('submit', function(e) {
        $signupForm.hide();
        e.preventDefault();
        e.stopPropagation();
        if($(this).find('#signup-password').val()!=$(this).find('#signup-password-confirmation').val()){
            alert("Passwords do not match");
            location.reload();
        }
        ll.signup(
            $(this).find('#signup-email').val(),
            $(this).find('#signup-password').val(),
            $(this).find('#signup-password-confirmation').val(),
            $(this).find('#signup-name').val(),
            $(this).find('#signup-city').val(),
            $(this).find('#signup-state').val(),
            $(this).find('#signup-street').val(),
            $(this).find('#signup-zip').val(),
            $(this).find('#signup-phone').val()
            );
        console.log(ll);
        $('#signup-email').val("").blur();
        $('#signup-password').val("").blur();
        $('#signup-password-confirmation').val("").blur();
        $('#signup-password-name').val("").blur();
        $('#signup-password-city').val("").blur();
        $('#signup-password-state').val("").blur();
        $('#signup-password-street').val("").blur();
        $('#signup-password-zip').val("").blur();
        $('#signup-password-phone').val("").blur();
    });

    $('#link-form').on('submit',function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.log("auth:",ll.auth);
        if (ll.auth) {
            console.log("current user", ll.auth);
            ll.submitLink($(this).find('#link-url').val(), $(this).find('#link-name').val(), ll.auth.uid);
            $(this).find("input[type=text]").val("").blur();
        }
        else {
            ll.onError("You need to signup or login to submit links!");
        }
        return false;
    });

    function showAlert(message, type) {
        var $alert = (
            $('<div>')                // create a <div> element
                .text(message)          // set its text
                .addClass('alert')      // add some CSS classes and attributes
                .addClass('alert-' + type)
                .addClass('alert-dismissible')
                .hide()  // initially hide the alert so it will slide into view
        );

        /* Add the alert to the alert container. */
        $alerts = $('#alerts');
        $alerts.append($alert);

        $alerts.on('click',function(e) {
            var $t=$(e.target);
            $t.remove();
        });

        /* Slide the alert into view with an animation. */
        $alert.slideDown();
        setTimeout(function(){$alert.hide()},3000);
    }
    $facebookLoginButton.on('click',function(e){
        ll.loginFacebook();
    });
    // ensure no user session is active
    ll.logout();
    // start firebase auth listener only after all callbacks are in place
    ll.start();

});